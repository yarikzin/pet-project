<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once('includes/header.php') ?>
	<main>
		<?php
		$user = mysqli_query($connection, "SELECT * FROM `users` where `user_id` = " . (int) $_GET['id']);

		if (mysqli_num_rows($user) > 0) {
			$art = mysqli_fetch_assoc($user);
		?>
			<table class="admin-table">
				<caption>Управление пользователем</caption>
				<thead>
					<tr>
						<th>ID</th>
						<th>login</th>
						<th>password</th>
						<th>gender</th>
						<th>age</th>
						<th>country</th>
						<th>city</th>
						<th>information</th>
						<th>user category</th>
						<th>delete user</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= $art['user_id'] ?></a></td>
						<td><?= $art['login'] ?></td>
						<td><?= $art['password'] ?></td>
						<td><?= $art['gender'] ?></td>
						<td><?= $art['age'] ?></td>
						<td><?= $art['country'] ?></td>
						<td><?= $art['city'] ?></td>
						<td><?= $art['info'] ?></td>
						<td><?= $art['user_cat'] ?></td>
						<td><a href="userManagement.php?action=delete_user&id=<?= $art['user_id'] ?>"><span class="text-danger">
									<ion-icon name="trash-outline"></ion-icon>
								</span></a></td>

					</tr>
				</tbody>
			</table>
		<?php
		}
		?>
		<div>
			<form action="manipulation/changeUserCat.php" method="POST" class="user_form">
				<caption>Изменить категорию покупателя</caption><br>
				<input type="hidden" name="user_id" value="<?= (int) $_GET['id'] ?>">
				<input type="radio" name="user_cat" value="1">Розничный (1)<br>
				<input type="radio" name="user_cat" value="2">Оптовый (2)<br>
				<input type="submit" name="change" value="Изменить"><br>
			</form>
		</div>
		<?php
		if (isset($_GET["action"])) {
			if ($_GET["action"] == "delete_user") {
				$action_del = mysqli_query($connection, "DELETE FROM `users` WHERE `user_id` = '" . $art['user_id'] . "'");
				echo "<script>alert('Пользователь удалён!');location.href='../../control.php'</script>";
			}
		}
		?>
	</main>
	<?php require_once "includes/footer.php" ?>
</body>

</html>