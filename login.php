<?php
require_once('includes/favicons.html');
require_once('includes/data_base.php');
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<?php if (isset($_GET['handleError'])) {
			echo "<script>alert('Неверный логин или пароль!');</script>";
		} ?>
		<div>
			<form method="POST" action="manipulation/handle.php" class="handle_form">
				<input class="input-login" type="text" placeholder="Логин" name="login" required>
				<input class="input-login" type="password" placeholder="Пароль" name="password" required>
				<button type="submit" class="handle_button">Войти</button>
			</form>
		</div>
	</main>
</body>

</html>