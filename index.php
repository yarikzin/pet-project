<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('test.php');
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<div class="owl-carousel owl-theme">
			<?php show_product($product, $res); ?>
		</div>
		<section class="top-side">
			<h2>Какой-то увлекательный заголовок</h2>
			<p>Далеко-далеко за словесными, горами в стране, гласных и согласных живут рыбные тексты. Запятой, проектах текста себя букв буквоград напоивший не сих щеке, дал. Пор своих продолжил, по всей языком за вершину переписали повстречался обеспечивает маленькая! Деревни над домах необходимыми назад имени букв! Маленький.</p>
			<p>Далеко-далеко за, словесными, горами в стране гласных и согласных живут рыбные тексты. Lorem большой все лучше по всей! Грустный сих даже обеспечивает толку взгляд рукописи буквоград снова назад послушавшись лучше своих lorem повстречался алфавит большой вопроса города продолжил, подпоясал моей на берегу пояс агентство но текст страну речью. Возвращайся не от всех если. Использовало, запятой?</p>
		</section>
		<section class="bottom-side">
			<h3>Менее захватывающий заголовок</h3>
			<p>Далеко-далеко за словесными, горами в стране гласных и согласных живут рыбные тексты. Коварных пустился, дорогу вершину повстречался снова осталось путь за продолжил, речью все толку подпоясал злых lorem пояс, ведущими? Предложения, если текстов. Речью назад правилами он коварный ipsum встретил обеспечивает строчка толку страна языком ему но безорфографичный пояс щеке рукопись себя заглавных взгляд силуэт своего, рекламных вопроса диких свою которой. Что заголовок переписывается не переулка предупредила о текстами снова?</p>
		</section>
	</main>
	<?php require_once "includes/footer.php" ?>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="js/owl.carousel.min.js"></script>

	<script>
		$(document).ready(function() {
			$(".owl-carousel").owlCarousel({
				items: 4,
				loop: true
			});
		});
	</script>
</body>

</html>