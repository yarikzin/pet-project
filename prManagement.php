<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once('includes/header.php') ?>
	<main>
		<?php
		$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY `product_id`");

		while ($res = mysqli_fetch_assoc($product)) :
		?>
			<div class="product-item">
				<a href="/editProd.php?id=<?= $res['product_id']; ?>"><img src='/images/<?php echo $res['image'] ?>'></a>
				<div class="product-list">
					<h3><?php echo $res['title'] ?></h3>
					<span class="price"><?php echo $res['price'] ?> UAH</span>
					<p><?php echo mb_substr(strip_tags($res['description']), 0, 9, 'utf-32') . ' ...'; ?></p>
				</div>
			</div>
		<?php
		endwhile;
		?>
	</main>
	<?php require_once "includes/footer.php" ?>
</body>

</html>