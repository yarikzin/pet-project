<?php
session_start();
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/cart.php');
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<h2>Доступные продукты в категории "Veev"</h2>
		<div class="sort_by_cat">
			<p class="sorting">Доступные категории:</p>
			<a href="else.php">Другое</a>
		</div>
		<div class="sort_by_brand">
			<p class="sorting">Бренды:</p>
			<a href="heets.php">Heets</a>
			<a href="fiit.php">Fiit</a>
		</div>
		<div class="sort">
			<form method='post' class="sort-form">
				<p class="sorting-select">Отсортировать по: </p>
				<select name='select'>
					<option value='a_z'>по имени (A-Z)</option>
					<option value='z_a'>по имени (Z-A)</option>
					<option value='priceMax'>от дешевых к дорогим</option>
					<option value='priceMin'>от дорогих к дешевым</option>
				</select>
				<button id="sort" type='submit' name='submit'>sort</button>
			</form>
		</div>
		<?php
		$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `brand` = 'Veev'");

		if ($_POST['select']) {

			$select = $_POST['select'];
			switch ($select) {
				case "a_z":
					$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `brand` = 'Veev' ORDER BY title");
					break;
				case "z_a":
					$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `brand` = 'Veev' ORDER BY title DESC");
					break;
				case "priceMax":
					$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `brand` = 'Veev'  ORDER BY price");
					break;

				case "priceMin":
					$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `brand` = 'Veev' ORDER BY price DESC");
					break;
			}
		}

		while ($res = mysqli_fetch_assoc($product)) :
		?>
			<div class="product-item">
				<a href="/productView.php?id=<?= $res['product_id']; ?>"><img src='/images/<?php echo $res['image'] ?>'></a>
				<div class="product-list">
					<h3><?php echo $res['title'] ?></h3>
					<span class="price"><?php echo $res['price'] ?> UAH</span>
					<p><?php echo mb_substr(strip_tags($res['description']), 0, 9, 'utf-32') . ' ...'; ?></p>
					<form action="product.php?action=add&id=<?php echo $res['product_id'] ?>" method="POST">
						<input type="hidden" name="quantity" class="form-control" value="1" />
						<input type="hidden" name="hidden_name" value="<?php echo $res['title'] ?>">
						<input type="hidden" name="hidden_price" value="<?php echo $res['price'] ?>">
						<input type="submit" name="add_to_cart" value="Добавить в корзину">
					</form>
				</div>
			</div>
		<?php
		endwhile;
		?>
		<?php if (!empty($_SESSION["shopping_cart"])) :  ?>
			<div class="table-responsive">
				<table class="table-bordered">
					<caption>
						<h3>Товары в корзине</h3>
					</caption>
					<tr>
						<th>Название продукта</th>
						<th>Количество</th>
						<th>Цена товара</th>
						<th>Сумма</th>
						<th>Действие</th>
					</tr>
					<?php
					$total = 0;
					foreach ($_SESSION["shopping_cart"] as $keys => $values) : ?>
						<tr>
							<td><?= $values["item_name"] ?></td>
							<td><?= $values["item_quantity"] ?></td>
							<td>UAH <?= $values["item_price"] ?></td>
							<td>UAH <?= number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
							<td><a href="product.php?action=delete&id=<?= $values["item_id"] ?>"><span class="text-danger">
										<ion-icon name="trash-outline"></ion-icon>
									</span></a></td>
						</tr>
					<?php $total = $total + ($values["item_quantity"] * $values["item_price"]);
					endforeach; ?>
					<tr>
						<td>Сума</td>
						<td>UAH <?= number_format($total, 2); ?></td>
						<td></td>
					</tr>
				<?php endif; ?>
				</table>
			</div>
	</main>
	<?php require_once "includes/footer.php" ?>

	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>
</body>

</html>