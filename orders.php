	<?php
	require_once('includes/favicons.html');
	require_once('includes/data_base.php');
	session_start();
	require_once('manipulation/all_about_user.php');
	protect_rage();
	?>
	<!DOCTYPE html>
	<html lang="ru">

	<head>
		<?php require_once('includes/head.php') ?>
	</head>

	<body>
		<?php require_once('includes/header.php') ?>
		<main>
			<table class="admin-table">
				<thead>
					<tr>
						<th>id</th>
						<th>client name</th>
						<th>phone</th>
						<th>mail</th>
						<th>address</th>
						<th>positions</th>
						<th>delete</th>
					</tr>
				</thead>
				<?php $orders = mysqli_query($connection, "SELECT * FROM `orders` ORDER BY `order_id`") ?>
				<?php while ($ord = mysqli_fetch_assoc($orders)) : ?>
					<tbody>
						<tr>
							<td><?= $ord['order_id'] ?></td>
							<td><?= $ord['client_name'] ?></td>
							<td><?= $ord['phone'] ?></td>
							<td><?= $ord['mail'] ?></td>
							<td><?= $ord['address'] ?></td>
							<td><?= $ord['positions'] ?></td>
							<td><a href="orders.php?action=delete_order&id=<?= $ord['order_id'] ?>"><span class="text-danger">
										<ion-icon name="trash-outline"></ion-icon>
									</span></a></td>
						</tr>
					</tbody>

					<?php if (isset($_GET["action"])) {
						if ($_GET['action'] == "delete_order") {
							$action = mysqli_query($connection, "DELETE FROM `orders` where `order_id` = " . $ord['order_id']);
							echo "<script>alert('Заказ удалён');location.href='../../orders.php'</script>";
							exit();
						}
					} ?>
				<?php endwhile ?>
			</table>
		</main>
		<?php require_once('includes/footer.php') ?>
	</body>

	</html>