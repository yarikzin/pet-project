<?php 
session_start();
require_once ('includes/data_base.php');

$name = $_POST['name'];
$phone = $_POST['phone'];
$mail = $_POST['mail'];
$address = $_POST['address'];

if (isset($_POST['stripeToken']) && !empty($_POST['stripeToken'])) {
  
    try {
        $token = $_POST['stripeToken'];
      
        $response = $gateway->purchase([
            'amount' => $_POST['amount'],
            'currency' => 'USD',
            'token' => $token,
        ])->send();
      
        if ($response->isSuccessful()) {
            $arr_payment_data = $response->getData();
            $payment_id = $arr_payment_data['id'];
            $amount = $_POST['amount'];
  
            $isPaymentExist = mysqli_query($connection, "SELECT * FROM payments WHERE payment_id = '".$payment_id."'");
      
            if($isPaymentExist->num_rows == 0) { 
                $insert = mysqli_query($connection, "INSERT INTO payments(payment_id, amount, currency, payment_status) VALUES('$payment_id', '$amount', 'USD', 'Captured')");
            }
  
			echo "<script>alert('Оплата прошла успешно. Номер вашего заказа: " . $payment_id . "');location.href='product.php';</script>";
        } else {
            echo $response->getMessage();
        }
    } catch(Exception $e) {
        echo $e->getMessage();
    }
}

	foreach($_SESSION["shopping_cart"] as $keys => $values) {
		$total = 0;
		$total = $total + ($values["item_quantity"] * $values["item_price"]);
		$total = number_format($total, 2);
		$time =  date(DATE_RFC822);		
		$id = $values['item_id'];
		$qty = $values['item_quantity'];
		$price = $values['item_price'];
		$unicId = uniqid();

		if ($_SESSION['user_cat'] == 2) {
			$sumWithoutTax = ($values['item_price'] - ($values['item_price'] / 100) * 3) * $values['item_quantity'] ;
			$tax = (($values['item_price'] / 100) * 3) * $values['item_quantity'] ;
		} else {
			$sumWithoutTax = ($values['item_price'] -  ($values['item_price'] / 100) * 5) * $values['item_quantity'] ;
			$tax = (($values['item_price'] / 100) * 5) * $values['item_quantity'] ;           	
		}

		$order = ("Время заказа: " . $time . ". " . " Данные о заказчике: " . $name . "; " . $phone . "; " . $mail . "; " . $address . ". " . " Данные о заказе: " . $values['item_name'] . " Id товара: " . $values['item_id'] . " Количество: " . $values['item_quantity'] . " Цена единицы: " . $values['item_price'] . " Сумма с налогом: " . $total . " Сумма без налога: " . $sumWithoutTax . " Налог: " . $tax . "." . PHP_EOL);
		$position = (" id: " . $values['item_id'] . " qty: " . $values['item_quantity'] . " unit price: " . $values['item_price'] . " sum with tax: " . $total . " sum without tax: " . $sumWithoutTax . " tax: " . $tax );

		$update = mysqli_query($connection, "INSERT IGNORE INTO orders (order_id, basket_id, client_name, phone, mail, address, item_id, qty, price, positions) VALUES ('$unicId', 0, '$name', '$phone', '$mail', '$address', '$id', '$qty', '$price', '$position' )");

		$fp = fopen("orders/" . $name . ".txt", 'a');
		fwrite($fp, $order);
		fclose($fp);
		
		$apiToken = "xxxxxxxxxxxxxxxxxxxx"; //ключ бота
		$data = [
			'chat_id' => 'xxxxxxxxx', // ключ юзера
			'text' => $order
		];
		$response = file_get_contents("https://api.telegram.org/bot$apiToken/sendMessage?" . http_build_query($data) );

		$order = "order#" . $_SESSION['name'];
		setcookie("$order" . "[id]", "", time()-5);
		setcookie("$order" . "[itemName]", "", time()-5);
		setcookie("$order" . "[itemPrice]", "", time()-5);
		setcookie("$order" . "[itemQty]", "", time()-5);

		unset($_SESSION['shopping_cart']);
		unset($_SESSION['shopping_cart.item_quantity']);
		unset($_SESSION['shopping_cart.qty']);
	}
