	<?php
	require_once('includes/favicons.html');
	require_once('includes/data_base.php');
	session_start();
	require_once('manipulation/all_about_user.php');
	protect_rage();
	?>
	<!DOCTYPE html>
	<html lang="ru">

	<head>
		<?php require_once('includes/head.php') ?>
	</head>

	<body>
		<?php require_once('includes/header.php') ?>
		<main>
			<?php if (isset($_GET['errorpasswod'])) {
				echo "<script>alert('Пароли не совпадают!');</script>";
			} ?>
			<form method="POST" action="manipulation/adminCreate.php" class="registration_form">
				<input type="text" placeholder="Логин" name="login" required minlength="6"><br>
				<input type="password" placeholder="Пароль" name="password" required minlength="6"><br>
				<input type="password" placeholder="Подтверждение пароля" name="password_confirm" required minlength="6"><br>
				<input type="text" placeholder="Номер телефона" name="phone" required><br>
				<input type="text" placeholder="Почта" name="mail" required><br>
				<button type="submit" class="registration_button">Создать администратора</button>
			</form>
		</main>
		<?php
		require_once('includes/footer.php');
		?>
	</body>

	</html>