<?php
session_start();
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<div class="editArticle">
			<?php
			$articles = mysqli_query($connection, "SELECT * FROM `articles` WHERE `id` = " . (int) $_GET['id']);

			while ($art = mysqli_fetch_assoc($articles)) : ?>
				<article class="article_adm">
					<div class="article_info_adm">
						<p><?= $art['title']; ?></p>
						<?php if (isset($_GET['action'])) {
							if ($_GET['action'] == "delete_art") {
								$action = mysqli_query($connection, "DELETE FROM `articles` WHERE `id` = " . $art['id']);
								echo "<script>alert('Статья удалена');location.href='../../artManagement.php'</script>";
								exit();
							}
						} ?>
						<?php
						$categorie_q = mysqli_query($connection, "SELECT * FROM `articles_categories`");
						$categorie = [];
						while ($cat = mysqli_fetch_assoc($categorie_q)) {
							$categorie[] = $cat;
						}

						$art_cat = false;
						foreach ($categorie as $cat) {
							if ($cat['id'] == $art['categorie_id']) {
								$art_cat = $cat;
								break;
							}
						}
						?>
						<div class="article_info_meta_adm">
							<small>ID статьи: <?= $art['id'] ?></small>
							<small>Категория: <?= $art_cat['title']; ?></small>
							<small>Дата и время публикации: <?= $art['pubdate'] ?></small>
							<small>Удалить статью: <a href="editArticle.php?action=delete_art&id=<?= $art['id'] ?>"><span class="text-danger">
										<ion-icon name="trash-outline"></ion-icon>
									</span></a></small>
						</div>
						<div class="full-text-adm">
							<?= $art['text'] ?>
						</div>
				</article>
				<div>
					<form action="manipulation/sendEditArt.php" method="POST" class="create_article_form">
						<p>
						<h3>Редактировать статью</h3>
						</p>
						<p><input type="hidden" name="art_id" value="<?= (int) $_GET['id'] ?>"></p>
						<p><input type="text" name="title" placeholder="Название"></p>
						<p>Выберите категорию: </p>
						<p class="radio"><input type="radio" name="art_cat" value="1"> Программирование <br></p>
						<p class="radio"><input type="radio" name="art_cat" value="2"> Lifestyle <br></p>
						<p class="radio"><input type="radio" name="art_cat" value="3"> Другое <br></p>
						<p><textarea type="text" name="text" placeholder="Текст статьи" minlength="1500"></textarea></p>
						<p><input type="submit" name="change" value="Изменить"></p>
					</form>
				</div>
			<?php endwhile; ?>
	</main>
	<?php require_once "includes/footer.php" ?>
</body>

</html>