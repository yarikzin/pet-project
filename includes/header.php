
<?php

session_start();


if(strstr($_SERVER['REQUEST_URI'], "index")){ ?>
	<header>
		<div class = "container">
			<form action="index.php" class="Logo"><button>H2osus</button></form>
			<nav>
				<ul class="header_top_menu">
					<li><form action="login.php"><button>Авторизация</button></form></li>
					<li><form action="registration.php"><button>Регистрация</button></form></li>
					<li><form action="#"><button>Контакты</button></form></li> 
					<li><form action="admin.php"><button><ion-icon name="construct-outline"></ion-icon></button></form></li>
				</ul>
			</nav>
		</div>
	</header> 
<?php }

if(strstr($_SERVER['REQUEST_URI'], "login")){ ?>
	<header>
		<div class = "container">
			<form action="index.php" class="Logo"><button>H2osus</button></form>
			<nav>
				<ul class="header_top_menu">
					<li><form action="registration.php"><button>Регистрация</button></form></li>
					<li><form action="#"><button>Контакты</button></form></li>
				</ul>
			</nav>
		</div>
	</header>

<?php }

if(strstr($_SERVER['REQUEST_URI'], "registration")){ ?>
	<header>
		<div class = "container">
			<form action="index.php" class="Logo"><button>H2osus</button></form>
			<nav>
				<ul class="header_top_menu">
					<li><form action="login.php"><button>Авторизация</button></form></li>
					<li><form action="#"><button>Контакты</button></form></li>
				</ul>
			</nav>
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "privat")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul>
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>
		</div> 
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "publick_bg")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul>
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>            	
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "product")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "pr_prod")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul>
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>	
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "basket")){ ?>
	<header>
	<div class="container">
	<nav>
	<ul>
	<li><form action="personal_page.php"><button>Главная</button></form></li>
	<li><form action="privat.php"><button>Личный блог</button></form></li>
	<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
	<li><form action="product.php"><button>Продукты</button></form></li>
	<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
	<li><form action="setting.php"><button>Настройки</button></form></li>
	<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
	</ul>
	</nav>	
	</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "setting")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul>
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "article")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "pr_art")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "admin")){ ?>
	<header>
		<div class="container">
			<form action="index.php" class="Logo"><button>H2osus</button></form>
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "control")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="createUser.php"><button>Создать пользователя</button></form></li>
					<li><form action="createAdmin.php"><button>Создать Администратора</button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "userManagement")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="control.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "orders")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="control.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "prManagement")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="control.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "artManagement")){ ?>
	<header>
	<div class="container">
	<nav>
	<ul class="header_top_menu">
	<li><form action="control.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
	<li><form action="orders.php"><button>Заказы</button></form></li>
	<li><form action="prManagement.php"><button>Продукты</button></form></li>
	<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
	</ul>
	</nav>	
	</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "editProd")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="prManagement.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "editArticle")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="artManagement.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "editPrivatArticle")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="artManagement.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "createUser")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="control.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "createAdmin")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="control.php"><button><ion-icon name="arrow-back-outline"></ion-icon></button></form></li>
					<li><form action="orders.php"><button>Заказы</button></form></li>
					<li><form action="prManagement.php"><button>Продукты</button></form></li>
					<li><form action="artManagement.php"><button>Статьи</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php }

if(strstr($_SERVER['REQUEST_URI'], "personal_page")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class = "header_top_menu">
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "steek")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "else")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "heets")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "veev")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
<?php } 

if(strstr($_SERVER['REQUEST_URI'], "fiit")){ ?>
	<header>
		<div class="container">
			<nav>
				<ul class="header_top_menu">
					<li><form action="personal_page.php"><button>Главная</button></form></li>
					<li><form action="privat.php"><button>Личный блог</button></form></li>
					<li><form action="publick_bg.php"><button>Публичный блог</button></form></li>
					<li><form action="product.php"><button>Продукты</button></form></li>
					<li><form action="pr_prod.php"><button>Мои продукты</button></form></li>
					<li><form action="basket.php"><button class="busket"><ion-icon name="cart-outline"></ion-icon><p><?= $_SESSION['shopping_cart.qty'] ?></p></button></form></li>
					<li><form action="setting.php"><button>Настройки</button></form></li>
					<li><form action="exit.php"><button><ion-icon name="log-out-outline"></ion-icon></button></form></li>
				</ul>
			</nav>	
		</div>
	</header>
	<?php } ?>