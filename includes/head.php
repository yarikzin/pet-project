<head>
	<meta charset="UTF-32">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>H2osusShop</title>
	<link rel="stylesheet" href="style/allstyle.css">
	<!--  -->
	<link rel="stylesheet" href="style/owl.carousel.min.css">
	<link rel="stylesheet" href="style/owl.theme.default.min.css">
	<!--  -->
	<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
	<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
	<!-- include Jquery -->
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<!--  -->
	<script src="https://js.stripe.com/v3/"></script>
</head>