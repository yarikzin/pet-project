<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/all_about_user.php');
session_start();
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
    <?php require_once "includes/header.php" ?>
    <main>
        <?php
        $res = mysqli_query($connection, "SELECT * FROM `users` WHERE `login` = '" . $_SESSION['name'] . "'");
        ?>

        <?php
        while ($art = mysqli_fetch_assoc($res)) {

        ?>
            <div class="inf">
                <table class="tableBordered">
                    <caption>
                        <h2><?php echo $_SESSION['name'] ?>, тут собраны ваши личные данные.</h2>
                    </caption>
                    <tr>
                        <th>Имя</th>
                        <th>Пол</th>
                        <th>Возраст</th>
                        <th>Страна</th>
                        <th>Город</th>
                        <th>Общая инфа</th>
                    </tr>
                    <tr>
                        <td><?php echo $art['login'] ?></td>
                        <td><?php echo $art['gender'] ?></td>
                        <td><?php echo $art['age'] ?></td>
                        <td><?php echo $art['country'] ?></td>
                        <td><?php echo $art['city'] ?></td>
                        <td><?php echo $art['info'] ?></td>
                        <td><a href="setting.php?action=delete_inf&id=<?= $_SESSION['name'] ?>"><span class="text-danger">Удалить данные</span></a></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <aside>
            <form class="setting_form">
                <h3>Изменить данные</h3>
                <input type="text" name="country" class="country" placeholder="Страна">
                <input type="text" name="city" class="city" placeholder="Город">
                <input type="text" name="info" class="info" placeholder="Информация о вас">
                <input type="submit" name="change" class="change" value="Изменить">
            </form>
        </aside>
    </main>
    <?php require_once "includes/footer.php" ?>

    <script>
        $(document).ready(function() {
            $('input.change').on('click', function() {
                var country = $('input.country').val();
                var city = $('input.city').val();
                var info = $('input.info').val();

                $.ajax({
                        method: "POST",
                        url: "manipulation/changeset.php",
                        data: {
                            country: country,
                            city: city,
                            info: info
                        }
                    })
                    .done(function() {});

                $('input.country').val('');
                $('input.city').val('');
                $('input.info').val('');
            })
        });
    </script>
    <script src="js/mini_basket.js"></script>
    <script src="js/purchase.js"></script>
</body>

</html>