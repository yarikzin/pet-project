<?php
session_start();
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<div class="left">
			<h2>Публичные статьи:</h2>
			<?php
			$articles = mysqli_query($connection, "SELECT * FROM `articles` ORDER BY `id`");

			while ($art = mysqli_fetch_assoc($articles)) : ?>
				<article class="article">
					<div class="article__info">
						<a href="/editArticle.php?id=<?php echo $art['id']; ?> "><?php echo $art['title']; ?></a>
						<?php
						$categorie_q = mysqli_query($connection, "SELECT * FROM `articles_categories`");
						$categorie = [];
						while ($cat = mysqli_fetch_assoc($categorie_q)) {
							$categorie[] = $cat;
						}

						$art_cat = false;
						foreach ($categorie as $cat) {
							if ($cat['id'] == $art['categorie_id']) {
								$art_cat = $cat;
								break;
							}
						}
						?>
						<div class="article_info_meta">
							<small>Категория: <a href="/categorie.php?id=<?php echo $art_cat['id']; ?>"><?php echo $art_cat['title']; ?></a></small>
						</div>
						<div class="article_info_preview"><?php echo mb_substr(strip_tags($art['text']), 0, 200, 'utf-32') . ' ...'; ?></div>
					</div>
				</article>
			<?php endwhile; ?>
		</div>

		<div class="right">
			<h3>Приватные статьи:</h3>
			<?php
			$res = mysqli_query($connection, "SELECT * FROM `privat_articles` ORDER BY `id`");
			?>

			<?php
			while ($privat = mysqli_fetch_assoc($res)) {

			?>
				<article class="article">
					<div class="article_info">
						<a href="/editPrivatArticle.php?id=<?php echo $privat['id']; ?> "><?php echo $privat['title']; ?></a>
						<?php
						$categorie_q = mysqli_query($connection, "SELECT * FROM `articles_categories`");
						$categorie = array();
						while ($cat = mysqli_fetch_assoc($categorie_q)) {
							$categorie[] = $cat;
						}
						?>
						<?php
						$art_cat = false;
						foreach ($categorie as $cat) {
							if ($cat['id'] == $privat['categorie_id']) {
								$art_cat = $cat;
								break;
							}
						}
						?>

						<div class="article_info_meta">
							<small>Категория: <a href="/categorie.php?id=<?php echo $art_cat['id']; ?>"><?php echo $art_cat['title']; ?></a></small>
							<small>Автор: <?php echo $privat['author']; ?></small>
						</div>
						<div class="article_info_preview"><?php echo mb_substr(strip_tags($privat['text']), 0, 200, 'utf-32') . ' ...'; ?></div>
					</div>
				</article>
			<?php
			}
			?>
		</div>
	</main>
	<?php require_once "includes/footer.php" ?>
</body>

</html>