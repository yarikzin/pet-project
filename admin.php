<?php
require_once('includes/favicons.html');
require_once('includes/data_base.php');
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<?php require_once('includes/head.php') ?>
</head>

<body>
	<?php require_once('includes/header.php'); ?>
	<main>
		<?php if (isset($_GET['handleError'])) {
			echo "<script>alert('Неверный логин или пароль!');</script>";
		} ?>
		<div>
			<form method="POST" action="manipulation/admin_handle.php" class="handle_form">
				<caption>Администрирование</caption>
				<input type="text" placeholder="Логин" name="login" required>
				<input type="password" placeholder="Пароль" name="password" required>
				<button type="submit" class="handle_button">Войти</button>
			</form>
	</main>
</body>

</html>