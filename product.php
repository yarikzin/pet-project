<?php
session_start();
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/cart.php');
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<div class="sort_by_cat">
			<p class="sorting">Доступные категории:</p>
			<a href="steek.php">Стики</a>
			<a href="else.php">Другое</a><br>
		</div>
		<div class="sort-else">
			<form method='post' class="sort-form">
				<p class="sorting-select">Отсортировать по: </p>
				<select name='select'>
					<option value='a_z'>по имени (A-Z)</option>
					<option value='z_a'>по имени (Z-A)</option>
					<option value='priceMax'>от дешевых к дорогим</option>
					<option value='priceMin'>от дорогих к дешевым</option>
				</select>
				<button id="sort" type='submit' name='submit'>sort</button>
			</form>
		</div>
		<h2>Доступные продукты:</h2>
		<?php
		$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY `product_id`");

		if ($_POST['select']) {

			$select = $_POST['select'];
			switch ($select) {
				case "a_z":
					$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY title");
					break;
				case "z_a":
					$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY title DESC");
					break;
				case "priceMax":
					$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY price");
					break;

				case "priceMin":
					$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY price DESC");
					break;
			}
		}

		while ($res = mysqli_fetch_assoc($product)) :
		?>
			<div class="product-item">
				<a href="/productView.php?id=<?= $res['product_id']; ?>"><img src='/images/<?php echo $res['image'] ?>'></a>
				<div class="product-list">
					<h3><?php echo $res['title'] ?></h3>
					<span class="price"><?php echo $res['price'] ?> UAH</span>
					<p><?php echo mb_substr(strip_tags($res['description']), 0, 9, 'utf-32') . ' ...'; ?></p>
					<form action="product.php?action=add&id=<?php echo $res['product_id'] ?>" method="POST">
						<input type="hidden" name="hidden_id" value="<?php echo $res['product_id']; ?>">
						<input type="hidden" name="quantity" class="form-control" value="1" />
						<input type="hidden" name="hidden_name" value="<?php echo $res['title'] ?>">
						<input type="hidden" name="hidden_price" value="<?php echo $res['price'] ?>">
						<input type="submit" name="add_to_cart" value="Добавить в корзину">
					</form>
				</div>
			</div>
		<?php
		endwhile;
		?>
	</main>
	<?php require_once "includes/footer.php" ?>
	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>
</body>

</html>