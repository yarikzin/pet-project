<?php
session_start();
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/cart.php');
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<?php
		$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `product_id` = " . (int) $_GET['id']);

		while ($res = mysqli_fetch_assoc($product)) :
		?>
			<div class="product-view">
				<img src='/images/<?php echo $res['image'] ?>'><br>
				<p>Описание товара: <?php echo $res['description'] ?></p><br>
			</div>
			<div class="product">
				<h3><?php echo $res['title'] ?></h3>
				<span class="price-view"><?php echo $res['price'] ?> UAH</span>
				<form action="product.php?action=add&id=<?php echo $res['product_id'] ?>" method="POST">
					<input type="hidden" name="quantity" class="form-control" value="1" />
					<input type="hidden" name="hidden_name" value="<?php echo $res['title'] ?>">
					<input type="hidden" name="hidden_price" value="<?php echo $res['price'] ?>">
					<input type="submit" name="add_to_cart" value="Добавить в корзину">
				</form>
			</div>
		<?php
		endwhile;
		?>
	</main>
	<?php require_once "includes/footer.php" ?>
	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>
</body>

</html>