<?php
session_start();
require_once __DIR__ . '../../includes/data_base.php';

if (isset($_GET['action'])) {
    if ($_GET['action'] == "add") {
        try {
            if (isset($_SESSION["shopping_cart"][$_GET['id']])) {
                $_SESSION["shopping_cart"][$_GET['id']]['item_quantity'] += 1;
            } else {
                $_SESSION["shopping_cart"][$_GET['id']] = [
                    'item_id' => $_GET['id'],
                    'item_name' => $_POST['hidden_name'],
                    'item_price' => $_POST['hidden_price'],
                    'item_quantity' => 1,
                ];

                $login = $_SESSION['name'];
                $rewiev = mysqli_query($connection, "SELECT * FROM users WHERE login = '$login' ");
                if ($rewiev = 1) {
                    $cuka = $_GET['id'];
                    $action = mysqli_query($connection, "UPDATE users SET cookie = '$cuka' WHERE login = '$login' ");
                }

                $order = "order#" . $_SESSION['name'];
                setcookie("$order" . "[id]", $_GET['id'], time() + 604800, "/", "", 0);
                setcookie("$order" . "[itemName]", $_POST['hidden_name'], time() + 604800, "/", "", 0);
                setcookie("$order" . "[itemPrice]", $_POST['hidden_price'], time() + 604800, "/", "", 0);
                setcookie("$order" . "[itemQty]", 1, time() + 604800, "/", "", 0);
            }

            $_SESSION['shopping_cart.item_quantity'][$_GET['id']] = !empty($_SESSION['shopping_cart.item_quantity']) ? ++$_SESSION['shopping_cart.item_quantity'][$_GET['id']]  : 1;
            $_SESSION['shopping_cart.sum'] = !empty($_SESSION['shopping_cart.sum']) ? $_SESSION['shopping_cart.sum'] + $product['item_price'] : $product['item_price'];
            $_SESSION['shopping_cart.qty'] = array_sum($_SESSION['shopping_cart.item_quantity']);
        } catch (Exception $e) {
            echo "что-то пошло не так";
            exit();
        }
    }
}


$id = $_POST['id'];

if (isset($_SESSION['shopping_cart'])) {
    foreach ($_SESSION["shopping_cart"] as $keys => $values) {
        if ($values["item_id"] == $id) {
            unset($_SESSION["shopping_cart"][$keys]);
            unset($_SESSION['shopping_cart.item_quantity'][$id]);
            $_SESSION['shopping_cart.qty'] = array_sum($_SESSION['shopping_cart.item_quantity']);
            $order = "order#" . $_SESSION['name'];
            setcookie("$order" . "[id]", "", time() - 5);
            setcookie("$order" . "[itemName]", "", time() - 5);
            setcookie("$order" . "[itemPrice]", "", time() - 5);
            setcookie("$order" . "[itemQty]", "", time() - 5);
        }
    }
}
