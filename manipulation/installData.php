<?php

//migration, install scripts

require_once __DIR__ . '../../includes/data_base.php';

$product = mysqli_query($connection, "SELECT * FROM `app_version` ORDER BY `version_id`");

$art = mysqli_fetch_assoc($product);

$app_version = $art['version_id']; //1

if($app_version < 2){
    $product = mysqli_query($connection, "CREATE TABLE articles ( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255) NULL, text TEXT NULL, categorie_id INT NULL, pubdate  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, views INT NOT NULL DEFAULT 0 )");
    $product = mysqli_query($connection, "CREATE TABLE articles_categories ( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, title VARCHAR(100) NOT NULL)");
    $product = mysqli_query($connection, "CREATE TABLE orders (order_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, basket_id INT NOT NULL, client_name TEXT NULL, phone VARCHAR(20) NULL, mail VARCHAR(200) NULL, address VARCHAR(255) NULL, positions VARCHAR(1000) NULL)");
    $product = mysqli_query($connection, "CREATE TABLE privat_articles (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, author VARCHAR(100) NULL, title VARCHAR(255) NULL, `text` TEXT NULL, categorie_id INT NULL, pubdate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, views INT NOT NULL DEFAULT 0)");
    $product = mysqli_query($connection, "CREATE TABLE product (product_id INT NOT NULL, author VARCHAR(100) NULL, title VARCHAR(255) NULL, image VARCHAR(255) NULL, description TEXT NULL, category VARCHAR(200) NOT NULL, brand VARCHAR(200) NOT NULL , price FLOAT NULL)");
    $product = mysqli_query($connection, "CREATE TABLE users (user_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, login TEXT NOT NULL, password VARCHAR(100) NOT NULL, cookie VARCHAR(100) NOT NULL, gender VARCHAR(100) NOT NULL, age TINYINT NOT NULL, country VARCHAR(255) NULL, city VARCHAR(255) NULL, info VARCHAR(255) NULL, user_cat INT NULL )");
    $product = mysqli_query($connection, "CREATE TABLE admin (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, Login TEXT NOT NULL, password VARCHAR(100) NOT NULL, phone VARCHAR(20) NOT NULL, mail VARCHAR(200) NOT NULL)");
    $product = mysqli_query($connection, "CREATE TABLE payments (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, payment_id VARCHAR(255) NOT NULL, amount FLOAT NOT NULL, currency VARCHAR(255) NOT NULL, payment_status VARCHAR(255) NOT NULL, captured_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP)");


    $product = mysqli_query($connection, "INSERT INTO admin (id, login, password, phone, mail) VALUES (NULL, 'Test-admin', '1bc210b6910caf868cef92b2d5fbd90d', '0999999999', 'gmail.com')");
    $product = mysqli_query ($connection, "INSERT INTO users (user_id, login, password, cookie, gender, age, country, city, info, user_cat) VALUES (NULL, 'Test-user', 'cfd6e6574064007e2b5b231c20ccd6bb' ,'default' ,'male', 66, 'Ukraine','Kyiv', 'default', 1)");
    $product = mysqli_query($connection, "update app_version set version_id = 2");


}
else {
    echo "version is valid";
} 