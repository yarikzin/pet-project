<?php 
session_start();
require_once __DIR__ . '../../includes/data_base.php';

if (isset($_COOKIE["order#" . $_SESSION['name']])) {
    if (isset($_SESSION["shopping_cart"][$_COOKIE["order#" . $_SESSION['name']]['id']])) {
        $_SESSION["shopping_cart"][$_COOKIE["order#" . $_SESSION['name']]['id']]['item_quantity'] += 1;
    } else {
        $_SESSION["shopping_cart"][$_COOKIE["order#" . $_SESSION['name']]['id']] = [
            'item_id' => $_COOKIE["order#" . $_SESSION['name']]['id'],
            'item_name' => $_COOKIE["order#" . $_SESSION['name']]['itemName'],
            'item_price' => $_COOKIE["order#" . $_SESSION['name']]['itemPrice'],
            'item_quantity' => 1,
        ];

        header('location: ../../basket.php');
        exit();
    }
}
?>