<?php 
require_once __DIR__ . '../../includes/data_base.php';
session_start();


function logged_in(){
    return(isset($_SESSION['name'])) ? true : false;
}

function protect_rage($params = null){
    if (logged_in() === false) {
        echo "<script>alert('Вы должны авторизоваться в системе для просмотра этой страницы!');location.href='../../login.php';</script>";
    }
}

if (isset($_GET["action"])) {
    if ($_GET["action"] == "delete_inf") {
        $action_del = mysqli_query($connection, "DELETE FROM `users` WHERE `login` = '".$_SESSION['name']."'");
        header('location: ../../login.php');
    }
}
?>


<div class="mini-busket">
    <table class="table-bordered">  
     <tr>  
      <th>Название продукта</th>  
      <th>Количество</th>  
      <th>Цена товара</th>  
      <th>Сумма</th> 
      <th>Изменить</th> 
      <th>Удалить</th>  
  </tr>  
  <?php   
  if(!empty($_SESSION["shopping_cart"]))  
  {  
      $total = 0;
      $qty = 0;
      foreach($_SESSION["shopping_cart"] as $keys => $values)  
      {  
         ?>  
         <tr class="inf">  
          <td><?php echo $values["item_name"]; ?></td>  
          <td><?php echo $values["item_quantity"]; ?></td>  
          <td>UAH <?php echo $values["item_price"]; ?></td>  
          <td>UAH <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
          <td><a href="basket.php?action=add&id=<?php echo $values['item_id'] ?>"><ion-icon name="add-circle-outline"></ion-icon></a></td>
          <td><input type="hidden" class="miniID" value="<?php echo $values["item_id"]; ?>"><a class="miniDelete"><span class="text-danger"><ion-icon name="trash-outline"></ion-icon></span></a></td>  
      </tr>  
      <?php  
      $total = $total + ($values["item_quantity"] * $values["item_price"]); 
      $qty = $qty += $values["item_quantity"] ;
  }  
  ?>  
  <tr class="inf">  
      <td>Сума</td>  
      <td>UAH <?php echo number_format($total, 2); ?></td>    
  </tr>  
  <?php  
}  
?> 
</table>
<?php 
$user = mysqli_query($connection, "SELECT * FROM `users` WHERE `login` = '".$_SESSION['name']."'");
while( $art = mysqli_fetch_assoc($user)){
    $_SESSION['user_cat'] = $art['user_cat'];}
    ?>

    <div class="info">
        <p><h3>Укажите свои данные для покупки</h3></p>
        <p title="Розница- налог 5% с единицы. Опт- налог 3% с единицы. Для категории оптовых покупателей минимально кол-во единиц = 5">***</p>  
        <input class="miniName" type="text" name="name" placeholder="ФИО" required>
        <input class="miniPhone" type="text" name="phone" placeholder="Номер телефона" required>
        <input class="miniMail" type="text" name="mail" placeholder="эл.Почта" required>    
        <input class="miniAddress" type="text" name="address" placeholder="Адрес" required>
        <?php 
        if ($_SESSION['user_cat'] == 2 && $qty < 5) {
        
        } else {
            echo '<input class="miniBuy" type="submit" name="buy" value="Купить">';
        }
        ?>
    </div>
    <div class="go_to_dasket">
        <a href="../../basket.php">Продолжить в корзине</a>
    </div>
</div>




