<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<?php
		$article = mysqli_query($connection, "SELECT * FROM `privat_articles` where `id` = " . (int) $_GET['id']);

		if (mysqli_num_rows($article) <= 0) {
		?>
			<div class="block">
				<h3>Статья не найдена</h3>
				<div class="block_content">
					<div class="full-text">
						Запрашиваемая вами статья не существует.
					</div>
				</div>
			</div>
		<?php
		} else {
			$art = mysqli_fetch_assoc($article);
			mysqli_query($connection, "UPDATE `privat_articles` SET `views` = `views` + 1 WHERE `id` = " . (int) $art['id']);
		?>
			<div class="block">
				<div class="views">
					<h3><?php echo $art['title']; ?></h3>
					<div class="block_content">
						<div class="full-text">
							<?php echo $art['text']; ?>
						</div>
					</div>
					<p class="qty-views">Просмотров: <?php echo $art['views']; ?>
					<p>
				</div>
			</div>
		<?php
		}
		?>
	</main>
	<?php require_once "includes/footer.php" ?>
	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>
</body>

</html>