<?php
require_once('includes/data_base.php');
session_start();

$product = mysqli_query($connection, "SELECT * FROM `product` ORDER BY `product_id` DESC LIMIT 10");

function show_product($product, $res)
{
	while ($res = mysqli_fetch_assoc($product)) { ?>
		<div class="itm">
			<div class="product-itm">
				<a href="/productView.php?id=<?= $res['product_id']; ?>"><img src='/images/<?php echo $res['image'] ?>'></a>
				<div class="product-lst">
					<h3><?php echo $res['title'] ?></h3>
					<span class="price-uno"><?php echo $res['price'] ?> UAH</span>
					<p><?php echo mb_substr(strip_tags($res['description']), 0, 9, 'utf-32') . ' ...'; ?></p>
				</div>
			</div>
		</div>

<?php
	}
} ?>