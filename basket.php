<?php
session_start();
require_once('manipulation/cart.php');
require_once('includes/data_base.php');
require_once('includes/favicons.html');
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
  <?php require_once "includes/header.php" ?>
  <main>
    <div class="table-responsive">
      <?php if (isset($_COOKIE["order#" . $_SESSION['name']])) : ?>
        <div class="help">
          <p>Нажмите <button><a href="manipulation/next.php">сюда</a></button> для продолжения прошлой покупки.</p>
        </div>
      <?php endif ?>
      <table class="table-bordered">
        <tr>
          <th>Название продукта</th>
          <th>Количество</th>
          <th>Цена товара</th>
          <th>Сумма</th>
          <th>Изменить</th>
          <th>Удалить</th>
        </tr>
        <?php
        if (!empty($_SESSION["shopping_cart"])) {
          $total = 0;
          $qty = 0;
          foreach ($_SESSION["shopping_cart"] as $keys => $values) {
        ?>
            <tr class="info">
              <td><?php echo $values["item_name"]; ?></td>
              <td><?php echo $values["item_quantity"]; ?></td>
              <td>UAH <?php echo $values["item_price"]; ?></td>
              <td>UAH <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
              <td><a href="?action=add&id=<?php echo $values['item_id'] ?>">
                  <ion-icon name="add-circle-outline"></ion-icon>
                </a></td>
              <td><input type="hidden" class="id" value="<?php echo $values["item_id"]; ?>"><a class="delete"><span class="text-danger">
                    <ion-icon name="trash-outline"></ion-icon>
                  </span></a></td>
            </tr>
          <?php
            $total = $total + ($values["item_quantity"] * $values["item_price"]);
            $qty = $qty += $values["item_quantity"];
            $cat = $_SESSION['user_cat'];
          }
          ?>
          <tr class="info">
            <td>Сума</td>
            <td>UAH <?php echo number_format($total, 2); ?></td>
          </tr>
        <?php
        }
        ?>
      </table>

      <?php
      $user = mysqli_query($connection, "SELECT * FROM `users` WHERE `login` = '" . $_SESSION['name'] . "'");
      while ($art = mysqli_fetch_assoc($user)) {
        $_SESSION['user_cat'] = $art['user_cat'];
      }
      ?>

      <div class="info">
        <form class="make_purchase" action="purchase.php" method="post" id="payment-form">
          <h3>Укажите свои данные для покупки</h3>
          <p title="Розница- налог 5% с единицы. Опт- налог 3% с единицы. Для категории оптовых покупателей минимально кол-во единиц = 5">***</p>
          <input class="name" type="text" name="name" placeholder="ФИО" required>
          <input class="phone" type="text" name="phone" placeholder="Номер телефона" required>
          <input class="mail" type="text" name="mail" placeholder="эл.Почта" required>
          <input class="address" type="text" name="address" placeholder="Адрес" required>
          <?php
          if ($_SESSION['user_cat'] == 2 && $qty < 5) : ?>

          <?php else : ?>
            <div class="form-row">
              <input type="text" name="amount" placeholder="Enter Amount" />
              <label for="card-element">
                Credit or debit card
              </label>
              <div id="card-element">

              </div>

              <div id="card-errors" role="alert"></div>
            </div>
            <input class="buy" type="submit" name="buy" value="Купить">

          <?php endif ?>
        </form>
      </div>
  </main>
  <?php require_once "includes/footer.php" ?>
  <script src="card.js"></script>
  <script src="js/purchase.js"></script>

</body>

</html>