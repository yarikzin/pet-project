<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<div class="create_prod">
			<h4>Добавить товар</h4>
			<form method="POST" action="manipulation/send_prod.php" class="create_item">
				<p><input type="file" name="picture" multiple accept="image/*,image/jpeg"></p>
				<p><input type="text" name="title" placeholder="Название товара" required></p>
				<select name="category" required>
					<option disabled>Выберите категорию товара:</option>
					<option value="Steek">Стики</option>
					<option value="else">Другое</option>
				</select>
				<select name="brand" required>
					<option disabled>Выберите бренд товара:</option>
					<option value="Heets">Heets</option>
					<option value="Veev">Veev</option>
					<option value="Fiit">Fiit</option>
					<option value="HandMade">HandMade</option>
				</select>
				<p><input class="desc" type="text" name="desc" placeholder="Описание товара" required></p>
				<p><input type="number" name="price" placeholder="Цена товара" required></p>
				<p><input type="submit" name="do_post" value="Добавить"></p>
			</form>
		</div>
		<h2>Ваши продукты, <?= $_SESSION['name'] ?>:</h2>
		<?php
		$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `author` = '" . $_SESSION['name'] . "'");

		while ($res = mysqli_fetch_assoc($product)) :
		?>
			<div class="product-item">
				<img src='/images/<?php echo $res['image'] ?>'>
				<div class="product-list">
					<h3><?= $res['title'] ?></h3>
					<span class="price"><?php echo $res['price'] ?> UAH</span>
					<p><?php echo mb_substr(strip_tags($res['description']), 0, 9, 'utf-32') . ' ...'; ?></p>
				</div>
			</div>
		<?php
		endwhile;
		?>
	</main>
	<?php require_once "includes/footer.php" ?>
	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>
</body>

</html>