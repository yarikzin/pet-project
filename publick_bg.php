<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<h2><?php echo $_SESSION['name'] ?>, тут собраны статьи всех пользователей.</h2>
		<?php
		$articles = mysqli_query($connection, "SELECT * FROM `articles` ORDER BY `id` DESC");
		while ($art = mysqli_fetch_assoc($articles)) :
		?>
			<article class="article">
				<div class="article__info">
					<a href="/article.php?id=<?php echo $art['id']; ?> "><?php echo $art['title']; ?></a>
					<?php
					$categorie_q = mysqli_query($connection, "SELECT * FROM `articles_categories`");
					$categorie = [];
					while ($cat = mysqli_fetch_assoc($categorie_q)) {
						$categorie[] = $cat;
					}
					$art_cat = false;
					foreach ($categorie as $cat) :
						if ($cat['id'] == $art['categorie_id']) {
							$art_cat = $cat;
							break;
						}
					endforeach;
					?>
					<div class="article_info_meta">
						<small>Категория: <a href="/categorie.php?id=<?php echo $art_cat['id']; ?>"><?php echo $art_cat['title']; ?></a></small>
					</div>
					<div class="article_info_preview"><?php echo mb_substr(strip_tags($art['text']), 0, 200, 'utf-32') . ' ...'; ?></div>
				</div>
			</article>
		<?php endwhile; ?>
		<aside>
			<h3>Создать статью</h3>
			<form class="create_article_form">
				<input type="text" name="title" class="title" placeholder="Название статьи" required maxlength="150">
				<p>Выберите категорию</p>
				<select name="art_categorie" class="art_categorie">
					<option disabled>Выберите категорию:</option>
					<option value="1">Программирование</option>
					<option value="2">Lifestyle</option>
					<option value="3">Другое</option>
				</select>
				<textarea type="text" name="text" class="text" placeholder="Текст статьи" required minlength="1500"></textarea>
				<button type="submit" name="do_post" class="submit">Опубликовать</button>
			</form>
		</aside>
	</main>
	<?php require_once "includes/footer.php" ?>

	<script>
		$(document).ready(function() {
			$('button.submit').on('click', function() {
				var titleValue = $('input.title').val();
				var art_categorieValue = $('select.art_categorie').val();
				var textValue = $('textarea.text').val();

				$.ajax({
						method: "POST",
						url: "manipulation/send.php",
						data: {
							title: titleValue,
							art_categorie: art_categorieValue,
							text: textValue
						}
					})
					.done(function() {
					});

				$('input.title').val('');
				$('select.art_categorie').val('');
				$('textarea.text').val('');
			})
		});
	</script>
	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>
</body>

</html>