<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>

		<h2>Последние статьи:</h2>

		<?php
		$articles = mysqli_query($connection, "SELECT * FROM `articles` ORDER BY `id` DESC LIMIT 10");

		while ($art = mysqli_fetch_assoc($articles)) : ?>
			<article class="article">
				<div class="article__info">
					<a href="/article.php?id=<?php echo $art['id']; ?> "><?php echo $art['title']; ?></a>
					<?php
					$categorie_q = mysqli_query($connection, "SELECT * FROM `articles_categories`");
					$categorie = [];
					while ($cat = mysqli_fetch_assoc($categorie_q)) {
						$categorie[] = $cat;
					}

					$art_cat = false;
					foreach ($categorie as $cat) {
						if ($cat['id'] == $art['categorie_id']) {
							$art_cat = $cat;
							break;
						}
					}
					?>
					<div class="article_info_meta">
						<small>Категория: <a href="/categorie.php?id=<?php echo $art_cat['id']; ?>"><?php echo $art_cat['title']; ?></a></small>
					</div>
					<div class="article_info_preview">
						<?php echo mb_substr(strip_tags($art['text']), 0, 200, 'utf-32') . ' ...'; ?>
					</div>
				</div>
			</article>
		<?php endwhile; ?>
	</main>
	<?php require_once "includes/footer.php" ?>
	<script src="js/mini_basket.js"></script>
	<script src="js/purchase.js"></script>

</body>

</html>