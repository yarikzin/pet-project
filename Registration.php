<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once "includes/header.php" ?>
	<main>
		<?php if (isset($_GET['errorpasswod'])) {
			echo "<script>alert('Пароли не совпадают!');</script>";
		} ?>
		<form method="POST" action="manipulation/regis.php" class="registration_form">
			<input type="text" placeholder="Логин" name="login" required minlength="6"><br>
			<input type="password" placeholder="Пароль" name="password" required minlength="6"><br>
			<input type="password" placeholder="Подтвердите пароль" name="password_confirm" required minlength="6"><br>
			<p>Ваш пол:</p>
			<input type="radio" name="gender" value="male">Мужской
			<input type="radio" name="gender" value="female">Женский<br>
			<input type="number" name="age" placeholder="Ваш возраст" required><br>
			<input type="text" name="country" placeholder="Страна" required><br>
			<input type="text" name="city" placeholder="Город" required><br>
			<button type="submit" class="registration_button">Регистрация</button>
		</form>
	</main>
</body>

</html>