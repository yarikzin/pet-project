	<?php
	require_once('includes/favicons.html');
	require_once('includes/data_base.php');
	session_start();
	require_once('manipulation/all_about_user.php');
	protect_rage();
	?>
	<!DOCTYPE html>
	<html lang="ru">

	<head>
		<?php require_once('includes/head.php') ?>
	</head>

	<body>
		<?php require_once('includes/header.php') ?>
		<main>
			<?php $user = mysqli_query($connection, "SELECT * FROM `users` ORDER BY `user_id`") ?>
			<?php while ($inf = mysqli_fetch_assoc($user)) : ?>
				<div class="user_info">
					<table class="admin-table">
						<caption>Управление пользователем <?= $inf['login'] ?></caption>
						<thead>
							<tr>
								<th>ID</th>
								<th>login</th>
								<th>password</th>
								<th>gender</th>
								<th>age</th>
								<th>country</th>
								<th>city</th>
								<th>information</th>
								<th>user category</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><a class="user-href" href="/userManagement.php?id=<?php echo $inf['user_id']; ?>"><?= $inf['user_id'] ?></a></td>
								<td><?= $inf['login'] ?></td>
								<td><?= $inf['password'] ?></td>
								<td><?= $inf['gender'] ?></td>
								<td><?= $inf['age'] ?></td>
								<td><?= $inf['country'] ?></td>
								<td><?= $inf['city'] ?></td>
								<td><?= $inf['info'] ?></td>
								<td><?= $inf['user_cat'] ?></td>
							</tr>
						</tbody>
					</table>
				<?php endwhile ?>
				</div>
		</main>
		<?php
		require_once('includes/footer.php');
		?>
	</body>

	</html>