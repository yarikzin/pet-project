<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
    <?php require_once "includes/header.php" ?>
    <main>
        <h1><?php echo $_SESSION['name'] ?>, тут собраны все ваши статьи.</h1>
        <?php
        $res = mysqli_query($connection, "SELECT * FROM `privat_articles` WHERE `author` = '" . $_SESSION['name'] . "'");
        ?>

        <?php
        while ($art = mysqli_fetch_assoc($res)) {

        ?>
            <article class="article">
                <div class="article__info">
                    <a href="/pr_art.php?id=<?php echo $art['id']; ?> "><?php echo $art['title']; ?></a>
                    <?php
                    $categorie_q = mysqli_query($connection, "SELECT * FROM `articles_categories`");
                    $categorie = array();
                    while ($cat = mysqli_fetch_assoc($categorie_q)) {
                        $categorie[] = $cat;
                    }
                    ?>
                    <?php
                    $art_cat = false;
                    foreach ($categorie as $cat) {
                        if ($cat['id'] == $art['categorie_id']) {
                            $art_cat = $cat;
                            break;
                        }
                    }
                    ?>

                    <div class="article_info_meta">
                        <small>Категория: <a href="/categorie.php?id=<?php echo $art_cat['id']; ?>"><?php echo $art_cat['title']; ?></a></small>
                        <small>Автор: <?php echo $art['author']; ?></small>
                    </div>
                    <div class="article_info_preview">
                        <?php echo mb_substr(strip_tags($art['text']), 0, 200, 'utf-32') . ' ...'; ?>
                    </div>
                </div>
            </article>
        <?php
        }
        ?>
        <aside>
            <h3>Создать статью</h3>
            <form class="create_article_form">
                <input type="text" name="title" class="title" placeholder="Название статьи" maxlength="150" required>
                <select name="art_categorie" class="art_categorie">
                    <option disabled>Выберите категорию:</option>
                    <option value="1">Программирование</option>
                    <option value="2">Lifestyle</option>
                    <option value="3">Другое</option>
                </select> <br>
                <textarea type="text" name="text" class="text" placeholder="Текст статьи" minlength="1500" required></textarea> <br>
                <button type="submit" name="do_post" class="submit">Отправить</button>
            </form>
        </aside>

    </main>
    <?php require_once "includes/footer.php" ?>

    <script>
        $(document).ready(function() {
            $('button.submit').on('click', function() {
                var titleValue = $('input.title').val();
                var art_categorieValue = $('select.art_categorie').val();
                var textValue = $('textarea.text').val();

                $.ajax({
                        method: "POST",
                        url: "manipulation/send_pr.php",
                        data: {
                            title: titleValue,
                            art_categorie: art_categorieValue,
                            text: textValue
                        }
                    })
                    .done(function() {});

                $('input.title').val('');
                $('select.art_categorie').val('');
                $('textarea.text').val('');
            })
        });
    </script>
    <script src="js/mini_basket.js"></script>
    <script src="js/purchase.js"></script>
</body>

</html>