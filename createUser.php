	<?php
	require_once('includes/favicons.html');
	require_once('includes/data_base.php');
	session_start();
	require_once('manipulation/all_about_user.php');
	protect_rage();
	?>
	<!DOCTYPE html>
	<html lang="ru">

	<head>
		<?php require_once('includes/head.php') ?>
	</head>

	<body>
		<?php require_once('includes/header.php') ?>
		<main>
			<?php if (isset($_GET['errorpasswod'])) {
				echo "<script>alert('Пароли не совпадают!');</script>";
			} ?>
			<form method="POST" action="manipulation/userCreate.php" class="registration_form">
				<input type="text" placeholder="Логин" name="login" required minlength="6"><br>
				<input type="password" placeholder="Пароль" name="password" required minlength="6"><br>
				<input type="password" placeholder="Подтверждение пароля" name="password_confirm" required minlength="6"><br>
				<p>Пол:</p>
				<input type="radio" name="gender" value="male">Мужской<br>
				<input type="radio" name="gender" value="female">Женский<br>
				<input type="number" name="age" placeholder="Возраст" required><br>
				<input type="text" name="country" placeholder="Страна" required><br>
				<input type="text" name="city" placeholder="Город" required><br>
				<input type="radio" name="user_cat" value="1">розница<br>
				<input type="radio" name="user_cat" value="2">опт<br>
				<button type="submit" class="registration_button">Создать пользователя</button>
			</form>
		</main>
		<?php
		require_once('includes/footer.php');
		?>
	</body>

	</html>