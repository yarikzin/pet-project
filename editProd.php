<?php
require_once('includes/data_base.php');
require_once('includes/favicons.html');
session_start();
require_once('manipulation/all_about_user.php');
protect_rage();
?>
<!DOCTYPE html>
<html lang="ru">
<?php require_once('includes/head.php') ?>

<body>
	<?php require_once('includes/header.php') ?>
	<main>
		<?php
		$product = mysqli_query($connection, "SELECT * FROM `product` WHERE `product_id` = " . (int) $_GET['id']);
		if (mysqli_num_rows($product)) {
			$art = mysqli_fetch_assoc($product); ?>

			<div class="product-red">
				<img src='/images/<?php echo $art['image'] ?>'>
			</div>

			<div class="product-red-one">
				<h3><?= $art['title'] ?></h3>
				<p class="auth-red">Автор: <?= $art['author'] ?></p>
				<p class="price-red"><span class="price"><?= $art['price'] ?> UAH</span></p>
				<p>Описание товара: <?= $art['description'] ?></p>
				<p class="del-red"><a class="del-red" href="editProd.php?action=delete_item&id=<?= $art['product_id'] ?>"><span class="text-danger">
							<ion-icon name="trash-outline"></ion-icon>
						</span></a></p>
			</div>

			<?php if (isset($_GET["action"])) {
				if ($_GET['action'] == "delete_item") {
					$action = mysqli_query($connection, "DELETE FROM `product` WHERE `product_id` = " . $art['product_id']);
					echo "<script>alert('Товар удалён');location.href='../../prManagement.php'</script>";
					exit();
				}
			} ?>

			<form action="manipulation/editItem.php" method="POST" class="red-item">
				<p><input type="hidden" name="item_id" value="<?= (int) $_GET['id'] ?>"></p>
				<p><input type="file" name="picture" multiple accept="image/*,image/jpeg"></p>
				<p><input type="text" name="title" placeholder="Название"></p>
				<input class="desc" type="text" name="description" placeholder="Описание">
				<input type="number" name="price" placeholder="Цена">
				<input class="author" type="text" name="author" placeholder="Автор">
				<p><input type="submit" name="change" value="Изменить"></p>
			</form>

		<?php } ?>
	</main>
	<?php require_once "includes/footer.php" ?>
</body>

</html>