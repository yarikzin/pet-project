App Version: 2.0.0;
Author Name: Yaroslav Zinenko.
Author email: Zinenkoaroslav@gmail.com.

Administrator login (Test-admin) and password (111111);
Default user login (Test-user) and password (123321);
Test payment details:
-card numbers (4242 4242 4242 4242);
- month/year (04/24);
- CVC (242);
- post index (42424);

Дамп бд:
- 'db/blog_db.sql';

Инициализация базы данных, при её  отсутствии:
1. Создать бд с таблицей 'app_version'.
2. В ней поля 'version_id', 'version'.
3. 'version_id' = произвольное значение.
4. 'version' = 1.
5. Запустить скрипт 'manipulation/InstallData.php'; 


Кажется должно работать...
